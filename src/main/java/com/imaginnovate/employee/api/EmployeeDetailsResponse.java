package com.imaginnovate.employee.api;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class EmployeeDetailsResponse {
    private int employeeCode;
    private String firstName;
    private String lastName;
    private double yearlySalary;
    private double taxAmount;
    private double cessAmount;
}
