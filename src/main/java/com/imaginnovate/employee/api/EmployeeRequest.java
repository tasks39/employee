package com.imaginnovate.employee.api;


import com.imaginnovate.employee.validator.ValidDate;
import com.imaginnovate.employee.validator.ValidSalary;
import lombok.Data;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.List;

@Data
public class EmployeeRequest {

    @NotBlank(message = "first Name is mandatory")
    private String firstName;
    @NotBlank(message = "last Name is mandatory")
    private String lastName;
    @NotBlank(message = "email is mandatory")
    @Pattern(regexp = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$", message = "Email not valid")
    private String email;
    @Size(min = 1, message = "minimum one phone no is required")
    private List<String> phones;
    @NotNull(message = "date of joining is mandatory")
    @ValidDate
    private LocalDate doj;
    @NotNull(message = "salary is mandatory")
    @ValidSalary
    private double salary;
}
