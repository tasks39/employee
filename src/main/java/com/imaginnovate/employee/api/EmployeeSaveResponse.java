package com.imaginnovate.employee.api;


import lombok.Data;

@Data
public class EmployeeSaveResponse {
    private String message;
}
