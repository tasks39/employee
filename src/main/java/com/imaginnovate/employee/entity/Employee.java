package com.imaginnovate.employee.entity;


import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Data
public class Employee implements Serializable {
    @Id
    @GeneratedValue
    private int employeeId;
    private String firstName;
    private String lastName;
    private String email;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "emp_id")
    private Set<Phone> phones;
    private LocalDate doj;
    private double salary;
}
