package com.imaginnovate.employee.utility;



import com.imaginnovate.employee.api.EmployeeRequest;
import com.imaginnovate.employee.entity.Employee;
import com.imaginnovate.employee.entity.Phone;

import java.util.HashSet;
import java.util.Set;

public class MapUtlity {

    public static Employee map(EmployeeRequest request){
        Employee e = new Employee();
        e.setDoj(request.getDoj());
        e.setEmail(request.getEmail());
        e.setFirstName(request.getFirstName());
        e.setLastName(request.getLastName());
        e.setSalary(request.getSalary());
        Set<Phone> phones = new HashSet<>();
        for(String number : request.getPhones()){
            Phone phone = new Phone();
            phone.setNumber(number);
            phones.add(phone);
        }
        e.setPhones(phones);
        return e;
    }
}
