package com.imaginnovate.employee.controller;



import com.imaginnovate.employee.api.EmployeeDetailsResponse;
import com.imaginnovate.employee.api.EmployeeRequest;
import com.imaginnovate.employee.api.EmployeeSaveResponse;
import com.imaginnovate.employee.entity.Employee;
import com.imaginnovate.employee.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/employee")
@RequiredArgsConstructor
public class EmployeeController {
    private final EmployeeService service;

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE)
    public EmployeeSaveResponse saveEmployee(@RequestBody @Valid EmployeeRequest employee){
        return service.saveEmployee(employee);
    }

    @GetMapping(value = "/employee-details", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EmployeeDetailsResponse>> getEmployee() {
        return ResponseEntity.ok(service.getEmployeeDetails());
    }

    @GetMapping("/all")
    public ResponseEntity<List<Employee>> getAllEmployee() {
        return ResponseEntity.ok(service.getAllEmployees());
    }
}
