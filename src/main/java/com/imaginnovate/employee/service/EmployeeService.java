package com.imaginnovate.employee.service;


import com.imaginnovate.employee.api.EmployeeDetailsResponse;
import com.imaginnovate.employee.api.EmployeeRequest;
import com.imaginnovate.employee.api.EmployeeSaveResponse;
import com.imaginnovate.employee.entity.Employee;
import com.imaginnovate.employee.repository.EmployeeRepository;
import com.imaginnovate.employee.utility.MapUtlity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDate;
import java.util.*;

@Component
@RequiredArgsConstructor
@Transactional
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    private static int TOTAL_MONTHS = 12;
    private static int DAYS_IN_MONTH = 30;
    private static double FIVE_PERCENT = 5;
    private static double TEN_PERCENT = 10;
    private static double TWENTY_PERCENT = 20;
    private static double CESS_PERCENT = 2;
    private static double PERCENT_DIVIDER = 100;
    private static double CESS_AMOUNT = 2500000;
    public EmployeeSaveResponse saveEmployee(EmployeeRequest employeereq) {
        var response = new EmployeeSaveResponse();
        Employee employee = MapUtlity.map(employeereq);
        employeeRepository.save(employee);

        response.setMessage("Employee saved sucessfully");
        return  response;
    }

    public List<EmployeeDetailsResponse> getEmployeeDetails() {

        List<Employee> employeeList = employeeRepository.findAll();
        List<EmployeeDetailsResponse> employeeDetailsResponses = new ArrayList<>();
        String[] strFinYear = getFinacialyear().split("-");
        for(Employee e : employeeList){
            double empSalary = e.getSalary();
            LocalDate doj = e.getDoj();
            int joined_Month = doj.getMonthValue();
            int joined_date = doj.getDayOfMonth();
            int joined_year = doj.getYear();
            int totalMonths = joined_year <  Integer.valueOf(strFinYear[0]) ? TOTAL_MONTHS : getmonths(TOTAL_MONTHS, joined_Month, joined_date);
            double year_salary = totalMonths * empSalary;
            year_salary = getYear_salary(strFinYear, empSalary, joined_date, joined_year, year_salary);
            double tax_amount = calculateTaxAmount(year_salary);
            double cess_amount = calculateCessAmount(year_salary);

            employeeDetailsResponses.add(addEployeeDetails(year_salary,tax_amount,cess_amount,e));
        }
        return employeeDetailsResponses;
    }


    private EmployeeDetailsResponse addEployeeDetails(double year_salary, double tax_amount, double cess_amount, Employee e) {
        return EmployeeDetailsResponse.builder()
                .employeeCode(e.getEmployeeId())
                .firstName(e.getFirstName())
                .lastName(e.getLastName())
                .yearlySalary(year_salary)
                .cessAmount(cess_amount)
                .taxAmount(tax_amount)
                .build();
    }

    private double getYear_salary(String[] strFinYear, double empSalary, int joined_date, int joined_year, double year_salary) {
        if(joined_year >=  Integer.valueOf(strFinYear[0]) && joined_year <=  Integer.valueOf(strFinYear[1])){
            double day_salary = empSalary /DAYS_IN_MONTH;
            int calculate_day = DAYS_IN_MONTH - joined_date;
            double remain_days_sal = day_salary * calculate_day;
            year_salary = year_salary + remain_days_sal;
        }
        return year_salary;
    }

    private double calculateCessAmount(double year_salary) {
        return year_salary > CESS_AMOUNT ?  (year_salary - CESS_AMOUNT) * (CESS_PERCENT/PERCENT_DIVIDER) : 0;
    }

    private double calculateTaxAmount(double year_salary) {
        double taxAmount = 0;
        if(year_salary <= 250000) {
            taxAmount = 0;
        }else if(year_salary > 250000 && year_salary <= 500000){
            taxAmount = year_salary * (FIVE_PERCENT/PERCENT_DIVIDER);
        }else if(year_salary > 500000 && year_salary <= 1000000){
            taxAmount = year_salary * (TEN_PERCENT/PERCENT_DIVIDER);
        }else{
            taxAmount = year_salary * (TWENTY_PERCENT/PERCENT_DIVIDER);
        }
       return taxAmount;
    }



    private int getmonths(int totalMonths, int joined_month, int joined_date) {
        int result;
        switch (joined_month){
            case 4:
                result = TOTAL_MONTHS;
                break;
            case 5:
                result = (TOTAL_MONTHS - 4) + 3;
                break;
            case 6:
                result = (TOTAL_MONTHS - 5) + 3;
                break;
            case 7:
                result = (TOTAL_MONTHS - 6) + 3;
                break;
            case 8:
                result = (TOTAL_MONTHS - 7) + 3;
                break;
            case 9:
                result = (TOTAL_MONTHS - 8) + 3;
                break;
            case 10:
                result = (TOTAL_MONTHS - 9) + 3;
                break;
            case 11:
                result = (TOTAL_MONTHS - 10) + 3;
                break;
            case 12:
                result = (TOTAL_MONTHS - 11) + 3;
                break;
            default:
                result = 0;
                break;
        }
        if(joined_date > 1){
            result = result - 1;
        }
        return result;
    }

    private String getFinacialyear() {
        LocalDate date = LocalDate.now();
        int year = date.getYear();
        int month = date.getMonthValue();
        return month > 3 ? year+"-"+(year+1) : (year-1)+"-"+year;
    }

    public List<Employee> getAllEmployees() {
        return employeeRepository
                .findAll();
    }

}
