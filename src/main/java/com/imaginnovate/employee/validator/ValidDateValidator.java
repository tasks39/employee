package com.imaginnovate.employee.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class ValidDateValidator implements ConstraintValidator<ValidDate, LocalDate> {
    @Override
    public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
        LocalDate systemDate = LocalDate.now();
        return (systemDate.getYear() >= value.getYear()) ? true : false;
    }
}
