package com.imaginnovate.employee.validator;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
@Constraint(validatedBy = ValidDateValidator.class)
public @interface ValidDate {
    String message() default "Year must be less than or current year";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
