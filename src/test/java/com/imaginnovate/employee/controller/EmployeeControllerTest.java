package com.imaginnovate.employee.controller;

import com.imaginnovate.employee.api.EmployeeRequest;
import com.imaginnovate.employee.api.EmployeeSaveResponse;
import com.imaginnovate.employee.service.EmployeeService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
class EmployeeControllerTest {


    @Mock
    private EmployeeService service;

    private EmployeeController employeeController;


    @BeforeEach
    void setUp(){
        employeeController = new EmployeeController(service);
    }

    @Test
    void testSaveEmploy(){
        var employeeResponse = new EmployeeSaveResponse();
        employeeResponse.setMessage("save suceesfully");
        when(service.saveEmployee(new EmployeeRequest())).thenReturn(employeeResponse);
        EmployeeSaveResponse response = employeeController.saveEmployee(new EmployeeRequest());
        Assertions.assertThat(response).isNotNull();
        Assertions.assertThat(response.getMessage()).isEqualTo("save suceesfully");
    }

}