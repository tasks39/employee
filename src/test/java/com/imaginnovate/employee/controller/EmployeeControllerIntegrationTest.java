package com.imaginnovate.employee.controller;

import com.imaginnovate.employee.api.EmployeeDetailsResponse;
import com.imaginnovate.employee.api.EmployeeRequest;
import com.imaginnovate.employee.api.EmployeeSaveResponse;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import java.time.LocalDate;
import java.util.Arrays;




@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "30000")
class EmployeeControllerIntegrationTest {

    /*User user = new User();
    then alt+enter on User
    will generate following
    user.setName("");
    user.setPassword("");*/

    @Autowired
    private WebTestClient webTestClient;

    @Test
    void testsaveEmployeewithdata(){
        EmployeeRequest employeeRequest = new EmployeeRequest();
        employeeRequest.setFirstName("venkatesh");
        employeeRequest.setLastName("koduri");
        employeeRequest.setEmail("venkateshkoduri91@gmail.com");
        employeeRequest.setPhones(Arrays.asList("9494172910"));
        employeeRequest.setDoj(LocalDate.now());
        employeeRequest.setSalary(92000);
        EntityExchangeResult<EmployeeSaveResponse> exchangeResult =  webTestClient
                .post().uri("/employee/save")
                .body(BodyInserters.fromValue(employeeRequest))
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody(EmployeeSaveResponse.class).returnResult();

        EmployeeSaveResponse response = exchangeResult.getResponseBody();
        Assertions.assertThat(response.getMessage()).isEqualTo("Employee saved sucessfully");

    }

    @Test
    void testsEmployeewithdata(){
        EntityExchangeResult<EmployeeDetailsResponse> exchangeResult =  webTestClient
                .get().uri("/employee/all")
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody(EmployeeDetailsResponse.class).returnResult();

        EmployeeDetailsResponse response = exchangeResult.getResponseBody();
        Assertions.assertThat(response.getFirstName()).isEqualTo("venkatesh");

    }

}